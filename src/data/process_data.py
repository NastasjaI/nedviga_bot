import click
import os
import pandas as pd

@click.group()
def cli():
    pass

@cli.command()
def analysis():
    from mymodule import lower, fill_null, fill_passes, remove_long_text

    if os.path.exists("../../data/raw/data.csv"):
        df_data = pd.read_csv('../../data/raw/data.csv')

        lower(df_data)

        fill_null(df_data, 'underground', 'district')
        fill_null(df_data, 'street', 'district')
        fill_null(df_data, 'district', 'underground')

        fill_passes(df_data)

        df_data = df_data.drop_duplicates()

        df_data['price_per_m2'] = df_data['price_per_m2'].abs()

        df_data['clean_district'] = df_data['district'].apply(remove_long_text)
        df_data.dropna(subset=['clean_district'], inplace=True)
        df_data.drop('district', axis=1, inplace=True)
        df_data = df_data.rename(columns={'clean_district' : 'district'})

        df_data = df_data.drop(['deal_type', 'accommodation_type', 'city', 'phone'], axis=1)

        try:
            os.unlink("../../data/interim/data.csv")
        except:
            print("Error while deleting file")

        df_data.to_csv(r'../../data/interim/data.csv', index=False)

        click.echo('Analysis data created successfully!')
    else:
        click.echo('File not found!')

@cli.command()
def prediction():
    import pandas as pd

    if os.path.exists("../../data/interim/data.csv"):
        df_data = pd.read_csv('../../data/interim/data.csv')

        df_data = df_data.drop(['street', 'author'], axis=1)

        df_data = df_data.query('rooms_count>0')

        # Ограничу площадь 9 кв. и 80% квартилем
        q90 = df_data['total_meters'].quantile(0.9)
        data1 = df_data.query('total_meters >9 and total_meters <= @q90')

        # определение 10% и 90% квартилей, чтобы всё, что выходит за рамки убрать
        q10 = data1['price_per_m2'].quantile(0.1)
        q80 = data1['price_per_m2'].quantile(0.8)
        data1 = data1.query('price_per_m2 >= @q10 and price_per_m2 <= @q80')

        # убираю цены, которые встречаются крайне редко
        data1 = data1.query('1000000<price<100000000')

        # Для обучения модели оставляем датасет без ссылок, номера телефона, жилой площади и площади кухни (данных по ним практически нет)
        data_model = data1.drop(['link'], axis=1)
        data_model.to_csv('data_model.csv', index=False)

        # Сохраняем таблицу с обработанными данными
        data1.to_csv(r'../../data/processed/prediction_data.csv', index=False)

        click.echo('Prediction data created successfully')
    else:
        click.echo('File not found!')


